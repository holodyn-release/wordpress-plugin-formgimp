<?php

/**
 * Optional Debugging
 * Included when WP_DEBUG is active
 */

defined('FORMGIMP_DEBUG') or define('FORMGIMP_DEBUG', true);

if( !function_exists('inspect') ){
  function inspect(){
    echo '<pre>' . print_r(func_get_args(), true) . '</pre>';
  }
}