<?php

/**
 * @wordpress-plugin
 * Plugin Name:       FormGimp
 * Plugin URI:        webuddha.com/formgimp
 * Description:       Formgimp for Wordpress. Create great forms!
 * Version:           1.1.1
 * Author:            webuddha
 * Author URI:        webuddha.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       formgimp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
  die;
}

/**
 * Require the core class
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-formgimp.php';

/**
 * Optional Debugging
 */
if (WP_DEBUG && is_readable(plugin_dir_path( __FILE__ ) . 'debug.php')) {
  require_once plugin_dir_path( __FILE__ ) . 'debug.php';
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
add_action('plugins_loaded',function(){
  Wp_Formgimp::getInstance()->init();
});

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-formgimp-activator.php
 */
register_activation_hook(__FILE__, function(){
  Wp_Formgimp::getInstance()->activate();
});

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-formgimp-deactivator.php
 */
register_deactivation_hook(__FILE__, function(){
  Wp_Formgimp::getInstance()->deactivate();
});
