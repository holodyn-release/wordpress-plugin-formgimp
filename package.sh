#!/bin/bash

rm formgimp.zip -f
zip formgimp.zip . -r -x *.git* -x package.sh -x formgimp.zip
