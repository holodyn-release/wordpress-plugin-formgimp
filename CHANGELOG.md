# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Change Tags

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

## 1.1.1 - 2021-10-21
### Added
- package icon

## 1.1.0 - 2021-10-20
### Changed
- updated formgimp library to 1.3.0

### Fixed
- media_root value set incorrectly

## 1.0.5 - 2020-02-19
### Added
- Session initialization
- Shortcode to form list/edit views

### Changed
- Admin menu to remove the repeated FormGimp submenu item
- Path assignment from get_template_xxx to get_stylesheet_xxx for child theme support
