if (typeof jQuery == 'function'){

  jQuery(document).ready(function(){
    jQuery('.formgimp.controller.forms_edit input[type=hidden][name=table]').each(function(){
      let formId = jQuery(this).val();
      jQuery(this).closest('td').append('<div style="display:inline-block;padding:.25rem .5rem;"><code>[formgimp form='+ formId +']</code></div>');
    });
    jQuery('.formgimp.controller.forms .data thead tr').each(function(){
      jQuery('<th width=1%>Shortcode</th>').insertAfter(jQuery(this).find('th:nth-child(2)'));
    });
    jQuery('.formgimp.controller.forms .data tbody tr').each(function(){
      let formId = jQuery(this).attr('data-form_table');
      jQuery('<td NOWRAP><code>[formgimp form='+ formId +']</code></td>').insertAfter(jQuery(this).find('td:nth-child(2)'));
    });
  });

}
