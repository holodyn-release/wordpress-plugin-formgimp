<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       webuddha.com
 * @since      1.0.0
 *
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/admin
 * @author     webuddha <dev@webuddha.com>
 */
class Wp_Formgimp_Admin {

  /**
   * The Plugin Instance.
   *
   * @since    1.0.0
   * @access   private
   * @var      object    $plugin    The Plugin Instance.
   */
  private $plugin;

  /**
   * The Loader Instance.
   *
   * @since    1.0.0
   * @access   private
   * @var      object    $loader    The Loader Instance.
   */
  private $loader;

  /**
   * Initialize the class and set its properties.
   *
   * @since    1.0.0
   * @param      string    $plugin_name       The name of this plugin.
   * @param      string    $version    The version of this plugin.
   */
  public function __construct($plugin) {

    // Stage
      $this->plugin      = $plugin;
      $this->loader      = $plugin->get_loader();

    // Hook
      $this->loader->add_action( 'admin_init', $this, 'admin_init'  );
      $this->loader->add_action( 'admin_menu', $this, 'admin_menu'  );
      $this->loader->add_action( 'admin_enqueue_scripts', $this, 'admin_enqueue_scripts' );
      $this->loader->add_filter( 'plugin_action_links_' . $this->plugin->get_plugin_basename(), $this, 'plugin_action_links' );

    // Push JS
      $this->plugin->scripts['js'][] = array( 'formgimp-admin', plugin_dir_url(__DIR__).'media/js/admin.js', array(), $this->plugin->get_version(), 'all' );

  }

  /**
   * Register Assets for the admin area.
   *
   * @since    1.0.0
   */
  public function admin_enqueue_scripts() {
    foreach ($this->plugin->scripts['js'] AS $js)
      wp_enqueue_script( $js[0], $js[1], $js[2], $js[3], $js[4] );
    foreach ($this->plugin->scripts['css'] AS $css)
      wp_enqueue_style( $css[0], $css[1], $css[2], $css[3], $css[4] );
  }

  /**
   * Register the administration menu for this plugin into the WordPress Dashboard menu.
   *
   * @since    1.0.0
   */
  public function admin_menu() {
    add_menu_page( 'FormGimp', 'FormGimp', 'manage_options', $this->plugin->get_plugin_name(), array($this, 'menu_display'));
    add_submenu_page( $this->plugin->get_plugin_name(), 'FormGimp Forms', 'Forms', 'manage_options', $this->plugin->get_plugin_name(), $this->plugin->get_plugin_name() . '-setup');
    add_submenu_page( $this->plugin->get_plugin_name(), 'FormGimp Config', 'Config', 'manage_options', $this->plugin->get_plugin_name() . '&task=setup', $this->plugin->get_plugin_name() . '-setup');
  }

   /**
   * Add settings action link to the plugins page.
   *
   * @since    1.0.0
   */
  public function plugin_action_links( $links ) {
    $settings_link = array(
      '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin->get_plugin_name() ) . '">' . __('Settings', $this->plugin->get_plugin_name()) . '</a>',
    );
    return array_merge(  $settings_link, $links );
  }

  /**
   * [admin_init description]
   * @return [type] [description]
   */
  public function admin_init(){
    if (@$_REQUEST['page'] == $this->plugin->get_plugin_name()) {
      $this->content = $this->plugin->callFormGimp();
    }
  }

  /**
   * Render the settings page for this plugin.
   *
   * @since    1.0.0
   */
  public function menu_display() {
    echo $this->content;
  }

}
