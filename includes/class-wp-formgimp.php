<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       webuddha.com
 * @since      1.0.0
 *
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/includes
 */

require_once plugin_dir_path(__DIR__) . 'includes/class-wp-formgimp-loader.php';
require_once plugin_dir_path(__DIR__) . 'includes/class-wp-formgimp-i18n.php';
require_once plugin_dir_path(__DIR__) . 'includes/class-wp-formgimp-admin.php';
require_once plugin_dir_path(__DIR__) . 'includes/class-wp-formgimp-public.php';
require_once plugin_dir_path(__DIR__) . 'lib/recaptcha/src/autoload.php';
require_once plugin_dir_path(__DIR__) . 'lib/Params/src/Params.php';
require_once plugin_dir_path(__DIR__) . 'lib/formgimp/autoload.php';

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/includes
 * @author     webuddha <dev@webuddha.com>
 */
class Wp_Formgimp {

  /**
   * Shared Instance
   * @var [type]
   */
  protected static $instance;

  /**
   * The unique identifier of this plugin.
   *
   * @since    1.0.0
   * @access   protected
   * @var      string    $plugin_name    The string used to uniquely identify this plugin.
   */
  protected $plugin_name = 'formgimp';

  /**
   * The current version of the plugin.
   *
   * @since    1.0.0
   * @access   protected
   * @var      string    $version    The current version of the plugin.
   */
  protected $version = '1.0.0';

  /**
   * The loader that's responsible for maintaining and registering all hooks that power
   * the plugin.
   *
   * @since    1.0.0
   * @access   protected
   * @var      Wp_Formgimp_Loader    $loader    Maintains and registers all hooks for the plugin.
   */
  protected $loader;

  /**
   * [$interface description]
   * @var [type]
   */
  protected $interface;

  /**
   * [$public_tasks description]
   * @var array
   */
  public $public_tasks = array('form','form.submit','form.result','download','captcha');

  /**
   * [$scripts description]
   * @var array
   */
  public $scripts = array(
    'js'  => array(),
    'css' => array()
    );

  /**
   * [$formGimpRequest description]
   * @var [type]
   */
  public $formGimpRequest;

  /**
   * Define the core functionality of the plugin.
   *
   * Set the plugin name and the plugin version that can be used throughout the plugin.
   * Load the dependencies, define the locale, and set the hooks for the admin area and
   * the public-facing side of the site.
   *
   * @since    1.0.0
   */
  public function __construct() {
  }

  /**
   * The name of the plugin used to uniquely identify it within the context of
   * WordPress and to define internationalization functionality.
   *
   * @since     1.0.0
   * @return    string    The name of the plugin.
   */
  public function get_plugin_name() {
    return $this->plugin_name;
  }

  public function get_plugin_basename(){
    return plugin_basename(plugin_dir_path(__DIR__) . $this->get_plugin_name() . '.php');
  }

  /**
   * Retrieve the version number of the plugin.
   *
   * @since     1.0.0
   * @return    string    The version number of the plugin.
   */
  public function get_version() {
    return $this->version;
  }

  /**
   * The reference to the class that orchestrates the hooks with the plugin.
   *
   * @since     1.0.0
   * @return    Wp_Formgimp_Loader    Orchestrates the hooks of the plugin.
   */
  public function get_loader() {
    if (!$this->loader) {
      $this->loader = new Wp_Formgimp_Loader();
    }
    return $this->loader;
  }

  /**
   * Define the locale for this plugin for internationalization.
   *
   * Uses the Wp_Formgimp_i18n class in order to set the domain and to register the hook
   * with WordPress.
   *
   * @since    1.0.0
   * @access   private
   */
  private function set_locale() {
    $plugin_i18n = new Wp_Formgimp_i18n();
    $this->get_loader()->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
  }

  /**
   * [getInstance description]
   * @return [type] [description]
   */
  public static function getInstance(){
    if (!self::$instance)
      self::$instance = new self();
    return self::$instance;
  }

  /**
   * Run the loader to execute all of the hooks with WordPress.
   *
   * @since    1.0.0
   */
  public function init() {
    $this->set_locale();
    $this->init_session();
    if (is_admin()) {
      $interface = new Wp_Formgimp_Admin($this);
    }
    else {
      $interface = new Wp_Formgimp_Public($this);
    }
    $this->get_loader()->run();
    $this->formGimpRequest = WebuddhaInc\FormGimp\Request::createFromGlobals();
  }

  /**
   * [init_session description]
   * @return [type] [description]

   * @since    1.0.5
   */
  public function init_session(){
    if (
      !headers_sent() &&
      php_sapi_name() !== 'cli' &&
      (version_compare(phpversion(), '5.4.0', '>=') ? session_status() !== PHP_SESSION_ACTIVE : session_id() === '')
      )
      session_start();
  }

  /**
   * [activate description]
   * @return [type] [description]
   */
  public function activate(){
  }

  /**
   * [deactivate description]
   * @return [type] [description]
   */
  public function deactivate(){
  }

  /**
   * Render Admin Form
   *
   * @since     1.0.0
   * @return    string    HTML String of requested Admin
   */
  public function callFormGimp( $task = null, $request = null ) {
    if (!$request)
      $request = $this->formGimpRequest;
    $app =
      new WebuddhaInc\FormGimp\App(
        new WebuddhaInc\FormGimp\DB($GLOBALS['wpdb']->dbh, 'wp_'),
        new WebuddhaInc\FormGimp\Config(array(
          'route_callback' => function($route){
            if (preg_match('/^(\/|[a-z]{3,4}\:\/\/)/', $route)) {
              return $route;
            }
            parse_str($route, $query);
            $admin = is_admin();
            if (@$query['task']) {
              $admin = true;
              if (in_array($query['task'], $this->public_tasks))
                $admin = false;
            }
            if ($admin)
              return get_admin_url() . 'admin.php?page='. $this->get_plugin_name() .'&' . $route;
            else
              return site_url() . '/index.php?page='. $this->get_plugin_name() .'&' . $route;
          },
          'media_callback' => function($type, $file, $params){
            $media_key = $this->get_plugin_name() . '-' . preg_replace('/\-+/','-',preg_replace('/[^A-Za-z0-9]/','-',preg_replace('/\.[A-Za-z0-9]+$/','',$file)));
            $url = preg_match('/^([a-z]+\:\/\/|\/)/',$file) ? $file : (plugin_dir_url(__DIR__).'lib/formgimp/src/media/' . $file);
            switch ($type) {
              case 'img':
                echo '<img src="'. $url .'" />';
                break;
              case 'css':
                if (did_action('wp_enqueue_scripts'))
                  wp_enqueue_style( $media_key, $url, array(), $this->get_version(), 'all' );
                else
                  $this->scripts['css'][] = array( $media_key, $url, array(), $this->get_version(), 'all' );
                break;
              case 'js':
                switch ($media_key) {
                  case $this->get_plugin_name() .'-js-jquery':
                    $media_key = 'jquery';
                    break;
                  case $this->get_plugin_name() .'-js-jquery-ui-core':
                    $media_key = 'jquery-ui-core';
                    break;
                  case $this->get_plugin_name() .'-js-jquery-ui-tooltip':
                    $media_key = 'jquery-ui-tooltip';
                    break;
                }
                if (did_action('wp_enqueue_scripts'))
                  wp_enqueue_script( $media_key, $url, array(), $this->get_version(), 'all' );
                else
                  $this->scripts['js'][] = array( $media_key, $url, array(), $this->get_version(), 'all' );
                break;
            }
          },
          'route_root' => '?page=' . $this->get_plugin_name(),
          'web_root'   => plugin_dir_url(__DIR__).'lib/formgimp/src',
          'media_root' => plugin_dir_url(__DIR__).'lib/formgimp/src/media',

          /**
           * TODO: Upgrade support for multiple paths
           *       Replaced get_template with get_stylesheet temporarily
           */
          // 'theme_path' => get_template_directory(),
          // 'theme_root' => get_template_directory_uri()

          'theme_path' => get_stylesheet_directory(),
          'theme_root' => get_stylesheet_directory_uri(),

          'lang_locale' => get_locale(),
          'lang_callback' => function($string, $params=null){
            return __( $string, 'formgimp' );
          }

          )),
        $request
      );
    return $app->render( @$task ?: @$_REQUEST['task'] ?: 'forms' );
  }

}
