<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       webuddha.com
 * @since      1.0.0
 *
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/includes
 * @author     webuddha <dev@webuddha.com>
 */
class Wp_Formgimp_i18n {

  /**
   * Load the plugin text domain for translation.
   *
   * @since    1.0.0
   */
  public function load_plugin_textdomain() {

    load_plugin_textdomain(
      'formgimp',
      false,
      dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
    );

  }

}
