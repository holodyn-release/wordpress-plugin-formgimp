<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       webuddha.com
 * @since      1.0.0
 *
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Formgimp
 * @subpackage Wp_Formgimp/public
 * @author     webuddha <dev@webuddha.com>
 */
class Wp_Formgimp_Public {

  /**
   * The Plugin Instance.
   *
   * @since    1.0.0
   * @access   private
   * @var      object    $plugin    The Plugin Instance.
   */
  private $plugin;

  /**
   * The Loader Instance.
   *
   * @since    1.0.0
   * @access   private
   * @var      object    $loader    The Loader Instance.
   */
  private $loader;

  /**
   * Initialize the class and set its properties.
   *
   * @since    1.0.0
   * @param      string    $plugin_name       The name of this plugin.
   * @param      string    $version    The version of this plugin.
   */
  public function __construct($plugin) {

    // Stage
      $this->plugin = $plugin;
      $this->loader = $plugin->get_loader();

    // Hook
      // TODO: Determine why this change was required for WP5
      // $this->loader->add_action( 'parse_query', $this, 'parse_query' );
      // $this->loader->add_action( 'wp_loaded', $this, 'page_request' );
      $this->loader->add_action( 'wp', $this, 'page_request' );

      $this->loader->add_action( 'wp_enqueue_scripts', $this, 'wp_enqueue_scripts' );
      $this->loader->add_filter( 'template_include', $this, 'template_include' );
      $this->loader->add_filter( 'the_content', $this, 'the_content' );

    // Shortcode
      add_shortcode($this->plugin->get_plugin_name(), array($this, 'display_shortcode'));

  }

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function wp_enqueue_scripts() {
    foreach ($this->plugin->scripts['js'] AS $js)
      wp_enqueue_script( $js[0], $js[1], $js[2], $js[3], $js[4] );
    foreach ($this->plugin->scripts['css'] AS $css)
      wp_enqueue_style( $css[0], $css[1], $css[2], $css[3], $css[4] );
	}

  /**
   * [is_plugin_call description]
   * @return boolean [description]
   */
  public function is_plugin_call(){
    return @$_REQUEST['page'] == $this->plugin->get_plugin_name();
  }

  /**
   * [page_request description]
   * @return [type] [description]
   */
  public function page_request(){
    if ($this->is_plugin_call()) {
      $request = clone $this->plugin->formGimpRequest;
      $task = $request->request->get('task', 'form');
      $task = in_array($task, $this->plugin->public_tasks) ? $task : 'form';
      $this->content = Wp_Formgimp::getInstance()->callFormGimp($task, $request);
    }
  }

  /**
   * [parse_query description]
   * @param  [type] &$query [description]
   * @return [type]         [description]
   */
  public function parse_query( &$query ){
    if ($this->is_plugin_call()) {
      $query->set('page_id', null);
      $query->set('posts_per_page', 1);
      $query->is_singular = false;
      $query->is_page     = false;
      $query->is_home     = false;
      $query->posts       = null;
    }
  }

  public function template_include( $template ){
    if ($this->is_plugin_call()) {
      $form_id = preg_replace('/[^a-z0-9\-\_]/','',strtolower(@$_REQUEST['form_id'] ?: (@$_REQUEST['table'] ?: null)));
      if ($form_id)
        $templates[] = 'page-'.$form_id.'.php';
      $templates[] = 'page-'.$this->plugin->get_plugin_name().'.php';
      $templates[] = 'page.php';
      return get_query_template( 'page', $templates );
    }
    return $template;
  }

  public function the_content( $content ){
    if ($this->is_plugin_call()) {
      return $this->content;
    }
    return $content;
  }

  /**
   * Render the settings page for this plugin.
   *
   * @since    1.0.0
   */
  public function display_shortcode($params) {
    if (isset($params['form']) && empty(isset($params['form_id']))) {
      $params['form_id'] = $params['form'];
      unset($params['form']);
    }
    $fieldData = array();
    foreach ($params AS $paramKey => $paramVal)
      if (preg_match('/^field_(.+)$/', $paramKey, $match))
        $fieldData[$match[1]] = $paramVal;
    $request = clone $this->plugin->formGimpRequest;
    $request
      ->request
      ->merge(array('form_id' => 'default', 'field' => $fieldData))
      ->merge($params);
    $task = $request->request->get('task', 'form');
    $task = in_array($task, $this->plugin->public_tasks) ? $task : 'form';
    return Wp_Formgimp::getInstance()->callFormGimp($task, $request);
  }

}
