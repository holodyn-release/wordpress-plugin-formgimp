
# v1

- [ ] Wordpress $_GET, $_POST, and $_REQUEST values are altered with addslashes().
	- [ ] Strip those back out from request before passing to the app.
	- [ ] Remove the implicit key filter on RequestObject.
	- [ ] Implement sanitation within the app.
