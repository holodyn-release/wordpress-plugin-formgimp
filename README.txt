=== FormGimp ===
Contributors: webuddha
Donate link: webuddha.com
Tags: forms, contact, contact form. free
Requires at least: 5.3.0
Tested up to: 5.8.1
Stable tag: 5.8.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create custom forms and save submissions to the database with FormGimp.  Receive notifications with a submission detail view link. Supports shortcodes.  Supports custom form post-submission processing.  Can be easily styled and customized with theme overrides.

== Description ==

FormGimp is a form builder we've been using for a decade.  It provides all the basic form building functionality you need, with the option to expand functionality quickly with custom form rendering, post submission processing, and email templates.

At a glance:

* Form Captcha / reCaptcha
* Build forms quickly, drag and drop field order
* Insert forms into posts using a shortcode
* Store submissions to the database
* Email Confirmation and Alert messages

== Installation ==

1. Upload `formgimp` to the `/wp-content/plugins/` directory
2. Activate the `FormGimp` plugin through the 'Plugins' menu in WordPress
3. Place the shortcode `[formgimp form=contact]` into a post to get started

== Frequently Asked Questions ==

= Is FormGimp amazing? =

It's a useful tool for basic form management.  If you need it to sing and dance then look elsewhere.

= Is FormGimp free? =

Yes, and we plan to keep it that way.  This package uses a common FormGimp core that we also use with other wrappers.

== Screenshots ==

1. Manage Forms
2. Create / Edit Form
3. Public Form Display
4. Public Form Result Display
5. Manage Form Submissions
6. View Form Submission

== Changelog ==

= 1.0 =
* Initial Release

= 1.0.1 .. 4 =
* Patches exposed during plugin submission

= 1.0.5 =
* Added new date, time, number field types
* Added form publish start/end date feature
* Added form submission maximum feature
* Added form duplicate prevention feature
* Added per-form confirmation message override
* Added shortcode display to form list/edit views
* Bug squashing and backend enhancements

== Upgrade Notice ==

= 1.0 =
This is the initial public release
